# AntibioticTerminalApp
The project will start by characterizing genome features that could be correlated with genes associated with antibiotic resistance. After creating a lot of usable (features) data the project will aim to develop a model in order to study patterns of antibiotic resistance in prokaryotic genome. Furthermore, different selection tests will be performed on interesting regions to investigate background forces that could drive this mutations. The project follows paper “Organised Genome Dynamics in the Escherichia coli Species Results in Highly Diverse Adaptive Paths” in order to focus on specific prokaryotic species such as Escherichia coli, Escherichia albertii and Shigella narrowing down to few strains. The final product of the project should be an algorithm (tool) that could be used in order to investigate possible regions and patterns of inputed genome that could correspond to antibiotic resistance.

I.	Bacterial Genome
A.	Escherichia Coli and Shigella 
B.	Resistance to antibiotics
II.	Bacterial Genome Features
A.	GC content - AT - GC ratio
B.	Frequencies
C.	Regions
III.	Gathering the Data
A.	Sources
B.	Problems
IV.	Building the model 
A.	All parameters
B.	Problems
C.	K - Nearest Neighbour
D.	Generalized linear model
V.	Shrinking The Model
A.	Finding most significant parameters
VI.	Significant Regions in genome
A.	Poisson Distribution
B.	Distribution comparison of resistant and normal genes 
VII.	Outcome - Command Line Tool
VIII.	Future tasks - Conclusion
A.	Phylogeny
B.	Optimization
C.	Other Machine learning methods
D.	Predicting resistance to specific group of antibiotics
E.	Using other bacterial strains and species
F.	Web Application - API
G.	Conclusion
IX.	References


